import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.Document;

import org.jsoup.Jsoup;

public class Main {
	public static void main(String[] args) {
		
		
		sample();
	}

	private static void sample(){
		JsoupSelectaClass frame = new JsoupSelectaClass();
		frame.view();
		frame.listener();
		
	}
	
	private static void sample2(){
		
		  JPanel panel = new JPanel();
	        JScrollPane scrollBar = new JScrollPane(panel,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	        JFrame frame = new JFrame("AddScrollBarToJFrame");
	        frame.add(scrollBar);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.setSize(400, 400);
	        frame.setVisible(true);
	        
	}
	
	private static void sample3(){
		String url = "http://stackoverflow.com";
		try {
			org.jsoup.nodes.Document document = Jsoup.connect(url).userAgent("Mozilla").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
