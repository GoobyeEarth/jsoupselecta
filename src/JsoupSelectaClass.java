import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.NoRouteToHostException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.jsoup.Jsoup;

import library.widget.JsoupControllerClass;

public class JsoupSelectaClass{
	
	
	private JTextField urlText;
	private org.jsoup.nodes.Document htmlDocument;
	private JButton goButton;
	private GridBagConstraints mainGridConstraints;
	
	private JLabel urlLoaded;
	private JPanel mainGridBugPanel;
	private JFrame frame;
	private JButton selectButton;
	private JTextField resultText;
	private JPanel scrollGridBugPanel;
	private GridBagConstraints scrollGridConstraints;
	
	private JTextArea htmlArea;
	private JButton htmlButton;
	public void view(){
		mainGridBugPanel = mainGridBugPanel();
		mainGridConstraints = new GridBagConstraints();
		mainGridConstraints.fill = GridBagConstraints.BOTH;
		
		JPanel htmlPanel = new JPanel();
		
		JScrollPane htmlScrollPane = setScrollPane(htmlPanel);
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 0;
		mainGridConstraints.weightx = 1;
		mainGridConstraints.weighty = 0.2;
		mainGridConstraints.gridwidth = 1;
		
		htmlArea = new JTextArea();
		htmlPanel.setLayout(new GridLayout(1, 1));
		htmlPanel.add(htmlArea);
		mainGridBugPanel.add(htmlScrollPane, mainGridConstraints);
		
		htmlButton = new JButton("load html");
		mainGridConstraints.gridx = 1;
		mainGridConstraints.gridy = 0;
		mainGridConstraints.weightx = 0;
		mainGridConstraints.weighty = 0;
		mainGridBugPanel.add(htmlButton, mainGridConstraints);
		
		
		
		urlText = new JTextField();
		urlText.setMaximumSize(new Dimension(2000, 30));
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 1;
		mainGridConstraints.weightx = 1;
		mainGridConstraints.weighty = 0;
		mainGridConstraints.gridwidth = 1;
		mainGridBugPanel.add(urlText, mainGridConstraints);

		goButton = new JButton("Go");
		mainGridConstraints.gridx = 1;
		mainGridConstraints.gridy = 1;
		mainGridConstraints.weightx = 0;
		mainGridBugPanel.add(goButton, mainGridConstraints);
		
		resultText = new JTextField();
		resultText.setMaximumSize(new Dimension(2000, 30));
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 2;
		mainGridConstraints.weightx = 1;
		mainGridBugPanel.add(resultText, mainGridConstraints);

		selectButton = new JButton("Select");
		mainGridConstraints.gridx = 1;
		mainGridConstraints.gridy = 2;
		mainGridConstraints.weightx = 0;
		mainGridBugPanel.add(selectButton, mainGridConstraints);
		
		
		scrollGridBugPanel = scrollGridBugPanel();
		JScrollPane scrollPane = new JScrollPane(scrollGridBugPanel,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMinimumSize(new Dimension(0, 200));
		scrollPane.setSize(new Dimension(0, 200));
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridwidth = 2;
		mainGridConstraints.gridy = 3;
		mainGridConstraints.weightx = 1;
		mainGridConstraints.weighty = 1;
		mainGridBugPanel.add(scrollPane, mainGridConstraints);
		
		
		
		scrollGridConstraints = new GridBagConstraints();
		scrollGridConstraints.fill = GridBagConstraints.BOTH;
		
		frame = new JFrame();
		frame.setSize(new Dimension(400, 400));
		frame.setTitle("JsoupSelecta");
		frame.setContentPane(mainGridBugPanel);
		frame.setVisible(true);
	}
	
	public JScrollPane setScrollPane(JPanel jPanel){
		JScrollPane scrollPane = new JScrollPane(jPanel,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMinimumSize(new Dimension(100, 200));
		scrollPane.setSize(new Dimension(100, 200));
		return scrollPane;
	}
	
	public JPanel mainGridBugPanel(){
		JPanel jPanel = new JPanel();
		GridBagLayout gridLayout = new GridBagLayout();
		jPanel.setLayout(gridLayout);
		
		return jPanel;
	}
	
	public JPanel scrollGridBugPanel(){
		JPanel jPanel = new JPanel();
		GridBagLayout gridLayout = new GridBagLayout();
		jPanel.setLayout(gridLayout);
		return jPanel;
	}
	
	public void listener(){
		ActionListener htmlLoadListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String html = htmlArea.getText();
				htmlDocument = Jsoup.parse(html);
				
			}
		};
		htmlButton.addActionListener(htmlLoadListener);
		
		ActionListener urlLoadListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String html = urlText.getText();
				try {
					htmlDocument = Jsoup.connect(html).get();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		};
		
		urlText.addActionListener(urlLoadListener);
		goButton.addActionListener(urlLoadListener);
		
		ActionListener contentsSearchListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String selecta = resultText.getText();
				List<JsoupControllerClass.ResultData> results = JsoupControllerClass.selectsGettingData(htmlDocument, selecta);
				
				scrollGridBugPanel.removeAll();
				
				int countList = 0;
				
				for (JsoupControllerClass.ResultData result : results) {
					
					JButton selectaButton = new JButton(result.indivisualSelecta);
					scrollGridConstraints.gridx = 0;
					scrollGridConstraints.gridy = countList;
					scrollGridConstraints.gridwidth = 1;
					scrollGridConstraints.weightx = 0;
					scrollGridBugPanel.add(selectaButton, scrollGridConstraints);
					countList += 1;
					
					selectaButton.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							Clipboard clipboard = Toolkit.getDefaultToolkit()
						            .getSystemClipboard();
						    StringSelection selection = new StringSelection(selecta);
						    clipboard.setContents(selection, selection);
						}
					});
					
					String[] formatedHtml = result.html.split("\n");
					for (String htmlLine : formatedHtml) {
						JLabel htmlJabel = new JLabel(htmlLine);
						scrollGridConstraints.gridx = 0;
						scrollGridConstraints.gridy = countList;
						scrollGridConstraints.gridwidth = 2;
						scrollGridConstraints.weightx = 1;
						scrollGridBugPanel.add(htmlJabel, scrollGridConstraints);
						countList += 1;
					}
					
					
					
				}
				Panel dummyPanel = new Panel();
				scrollGridConstraints.gridx = 0;
				scrollGridConstraints.gridy = countList;
				scrollGridConstraints.weightx = 1;
				scrollGridBugPanel.add(dummyPanel, scrollGridConstraints);

				scrollGridBugPanel.repaint();
				scrollGridBugPanel.revalidate();
			}
		};
		
		resultText.addActionListener(contentsSearchListener);
		selectButton.addActionListener(contentsSearchListener);
		
		
	}
	
	
	
	
	
	
	
	
	
	
}
